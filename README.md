Тестовая Задача:

1. Food ordering system

Imagine you have to design an application for a restaurant ordering system(no GUI, only back-end (REST APIs)). Please follow the requirements listed below:

Functional requirements:

● It’s possible to order lunch and/or drink.

● Lunch consists of a main course and dessert.

● Each meal and drink must have a name and price.

● There are three cuisines available to choose from (Polish, Mexican, Italian).

● When ordering a drink, the customer can additionally ask for ice cubes or/and lemon.

● The solution you come up with should be extendable (it should be possible to add new cuisines/dishes in the future).

● It should contain unit tests.

++ при выполнении данного ТЗ обязательно используем Spring boot, база H2

===

![Модель данных](docs/img/data_model.png)

===

CUISINS

GET - getAllCuisins
curl -v -H 'Accept: application/json' http://localhost:8080/cuisins

[{"id":1,"cuisin":"Polish","dish":"PolishDish1","price":200.0},{"id":2,"cuisin":"Polish","dish":"PolishDish2","price":200.0},{"id":3,"cuisin":"Polish","dish":"PolishDish3","price":200.0},{"id":4,"cuisin":"Mexican","dish":"MexixcanDish1","price":200.0},{"id":5,"cuisin":"Mexican","dish":"MexixcanDish2","price":200.0},{"id":6,"cuisin":"Mexican","dish":"MexixcanDish3","price":200.0},{"id":7,"cuisin":"Italian","dish":"ItalianDish1","price":200.0},{"id":8,"cuisin":"Italian","dish":"ItalianDish2","price":200.0},{"id":9,"cuisin":"Italian","dish":"ItalianDish3","price":200.0}]

GET - getCuisinById
curl -v -H 'Accept: application/json' http://localhost:8080/cuisins/1

{"id":1,"cuisin":"Polish","dish":"PolishDish1","price":200.0}

POST - addCuisin
curl -X POST -vd '{"id":null,"cuisin":"Polish","dish":"PolishDishNew","price":300.0}' -H 'Content-Type: application/json' http://localhost:8080/cuisins

{"id":10,"cuisin":"Polish","dish":"PolishDishNew","price":300.0}

UPDATE PUT - putCuisin
curl -X PUT -vd '{"id":4,"cuisin":"UpdatedMexicanCuisin","dish":"UpdatedMexicanDish","price":500.0}' -H 'Content-Type: application/json' http://localhost:8080/cuisins

{"id":4,"cuisin":"UpdatedMexicanCuisin","dish":"UpdatedMexicanDish","price":500.0}

UPDATE PATCH - patchCuisin
curl -X PATCH -vd '{"price":1000.0}' -H 'Content-Type: application/json' http://localhost:8080/cuisins/4

{"id":4,"cuisin":"UpdatedMexicanCuisin","dish":"UpdatedMexicanDish","price":1000.0}

DELETE - deleteCuisin
curl -X DELETE -v http://localhost:8080/cuisins/4

===

DESSERTS

GET - getAllDesserts
curl -v -H 'Accept: application/json' http://localhost:8080/desserts

[{"id":1,"name":"Dessert1","price":100.0},{"id":2,"name":"Dessert2","price":100.0},{"id":3,"name":"Dessert3","price":100.0}]

GET - getDessertById
curl -v -H 'Accept: application/json' http://localhost:8080/desserts/1

{"id":1,"name":"Dessert1","price":100.0}

POST - addDessert
curl -X POST -vd '{"id":null,"name":"NewDessert","price":300.0}' -H 'Content-Type: application/json' http://localhost:8080/desserts

{"id":4,"name":"NewDessert","price":300.0}

UPDATE PUT - putDessert
curl -X PUT -vd '{"id":3,"name":"UpdatedDessert","price":500.0}' -H 'Content-Type: application/json' http://localhost:8080/desserts

{"id":3,"name":"UpdatedDessert","price":500.0}

UPDATE PATCH - patchDessert
curl -X PATCH -vd '{"price":1000.0}' -H 'Content-Type: application/json' http://localhost:8080/desserts/3

{"id":3,"name":"UpdatedDessert","price":1000.0}

DELETE - deleteDessert
curl -X DELETE -v http://localhost:8080/desserts/3

===

DRINKS

GET - getAllDrinks
curl -v -H 'Accept: application/json' http://localhost:8080/drinks

[{"id":1,"name":"DRINK1","price":100.0},{"id":2,"name":"DRINK2","price":100.0},{"id":3,"name":"DRINK3","price":100.0}]

GET - getDrinkById
curl -v -H 'Accept: application/json' http://localhost:8080/drinks/1

{"id":1,"name":"DRINK1","price":100.0}

POST - addDrink
curl -X POST -vd '{"id":null,"name":"NewDrink","price":300.0}' -H 'Content-Type: application/json' http://localhost:8080/drinks

{"id":4,"name":"NewDrink","price":300.0}

UPDATE PUT - putDrink
curl -X PUT -vd '{"id":3,"name":"UpdatedDrink","price":500.0}' -H 'Content-Type: application/json' http://localhost:8080/drinks

{"id":3,"name":"UpdatedDrink","price":500.0}

UPDATE PATCH - patchDrink
curl -X PATCH -vd '{"price":1000.0}' -H 'Content-Type: application/json' http://localhost:8080/drinks/3

{"id":3,"name":"UpdatedDrink","price":1000.0}

DELETE - deleteDrink
curl -X DELETE -v http://localhost:8080/drinks/3

===

LUNCHS

GET - getAllLunchs
curl -v -H 'Accept: application/json' http://localhost:8080/lunchs

[{"id":1,"cuisinId":1,"dessertId":1},{"id":2,"cuisinId":2,"dessertId":2},{"id":3,"cuisinId":3,"dessertId":3},{"id":4,"cuisinId":4,"dessertId":1},{"id":5,"cuisinId":5,"dessertId":2},{"id":6,"cuisinId":6,"dessertId":3},{"id":7,"cuisinId":7,"dessertId":1},{"id":8,"cuisinId":8,"dessertId":2},{"id":9,"cuisinId":9,"dessertId":3}]

GET - getLunchById
curl -v -H 'Accept: application/json' http://localhost:8080/lunchs/1

{"id":1,"cuisinId":1,"dessertId":1}

POST - addLunch
curl -X POST -vd '{"id":null,"cuisinId":1,"dessertId":1}' -H 'Content-Type: application/json' http://localhost:8080/lunchs

{"id":10,"cuisinId":1,"dessertId":1}

UPDATE PUT - putLunch
curl -X PUT -vd '{"id":3,"cuisinId":1,"dessertId":1}' -H 'Content-Type: application/json' http://localhost:8080/lunchs

{"id":3,"cuisinId":1,"dessertId":1}

UPDATE PATCH - patchLunch
curl -X PATCH -vd '{"dessertId":2}' -H 'Content-Type: application/json' http://localhost:8080/lunchs/3

{"id":3,"cuisinId":1,"dessertId":2}

DELETE - deleteLunch
curl -X DELETE -v http://localhost:8080/lunchs/3

===

ORDERS

GET - getAllOrders
curl -v -H 'Accept: application/json' http://localhost:8080/orders

[{"id":1,"lunchId":1,"drinkId":1,"ice":true,"lemon":true,"totalPrice":400.0},{"id":2,"lunchId":1,"drinkId":1,"ice":true,"lemon":true,"totalPrice":400.0},{"id":3,"lunchId":1,"drinkId":1,"ice":true,"lemon":true,"totalPrice":400.0}]

GET - getOrderById
curl -v -H 'Accept: application/json' http://localhost:8080/orders/1

{"id":1,"lunchId":1,"drinkId":1,"ice":true,"lemon":true,"totalPrice":400.0}

POST - addOrder
curl -X POST -vd '{"id":null,"lunchId":1,"drinkId":1,"ice":"true","lemon":"true","totalPrice":0.0}' -H 'Content-Type: application/json' http://localhost:8080/orders

{"id":4,"lunchId":1,"drinkId":1,"ice":true,"lemon":true,"totalPrice":400.0}

UPDATE PUT - putOrder
curl -X PUT -vd '{"id":1,"lunchId":1,"drinkId":1,"ice":"false","lemon":"true","totalPrice":400.0}' -H 'Content-Type: application/json' http://localhost:8080/orders

{"id":1,"lunchId":1,"drinkId":1,"ice":false,"lemon":true,"totalPrice":400.0}

UPDATE PATCH - patchOrder
curl -X PATCH -vd '{"totalPrice":1000.0}' -H 'Content-Type: application/json' http://localhost:8080/orders/1

{"id":1,"lunchId":1,"drinkId":1,"ice":false,"lemon":true,"totalPrice":1000.0}

DELETE - deleteOrder
curl -X DELETE -v http://localhost:8080/orders/1
