package io.gitlab.ciskos.foodorderingsystem.converter;

import io.gitlab.ciskos.foodorderingsystem.dto.OrderDto;
import io.gitlab.ciskos.foodorderingsystem.model.drink.Drink;
import io.gitlab.ciskos.foodorderingsystem.model.meal.Lunch;
import io.gitlab.ciskos.foodorderingsystem.model.order.Order;

public class ConvertOrder {
	
	public static OrderDto toDto(Order order) {
		return new OrderDto(order.getId()
								, order.getLunch().getId()
								, order.getDrink().getId()
								, order.getIce()
								, order.getLemon()
								, order.getTotalPrice());
	}

	public static Order toEntity(OrderDto orderDTO, Lunch lunch, Drink drink) {
		return new Order(orderDTO.getId()
							, lunch
							, drink
							, orderDTO.getIce()
							, orderDTO.getLemon()
							, orderDTO.getTotalPrice());
	}

}
