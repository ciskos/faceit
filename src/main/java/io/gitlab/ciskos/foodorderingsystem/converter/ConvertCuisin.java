package io.gitlab.ciskos.foodorderingsystem.converter;

import io.gitlab.ciskos.foodorderingsystem.dto.CuisinDto;
import io.gitlab.ciskos.foodorderingsystem.model.meal.course.Cuisin;

public class ConvertCuisin {
	
	public static CuisinDto toDto(Cuisin cuisin) {
		return new CuisinDto(cuisin.getId()
								, cuisin.getCuisin()
								, cuisin.getDish()
								, cuisin.getPrice());
	}

	public static Cuisin toEntity(CuisinDto cuisinDTO) {
		return new Cuisin(cuisinDTO.getId()
							, cuisinDTO.getCuisin()
							, cuisinDTO.getDish()
							, cuisinDTO.getPrice());
	}

}
