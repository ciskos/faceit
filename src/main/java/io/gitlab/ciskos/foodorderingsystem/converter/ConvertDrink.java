package io.gitlab.ciskos.foodorderingsystem.converter;

import io.gitlab.ciskos.foodorderingsystem.dto.DrinkDto;
import io.gitlab.ciskos.foodorderingsystem.model.drink.Drink;

public class ConvertDrink {
	
	public static DrinkDto toDto(Drink drink) {
		return new DrinkDto(drink.getId()
								, drink.getName()
								, drink.getPrice());
	}

	public static Drink toEntity(DrinkDto drinkDTO) {
		return new Drink(drinkDTO.getId()
							, drinkDTO.getName()
							, drinkDTO.getPrice());
	}

}
