package io.gitlab.ciskos.foodorderingsystem.converter;

import io.gitlab.ciskos.foodorderingsystem.dto.LunchDto;
import io.gitlab.ciskos.foodorderingsystem.model.meal.Lunch;
import io.gitlab.ciskos.foodorderingsystem.model.meal.course.Cuisin;
import io.gitlab.ciskos.foodorderingsystem.model.meal.dessert.Dessert;

public class ConvertLunch {
	
	public static LunchDto toDto(Lunch lunch) {
		return new LunchDto(lunch.getId()
								, lunch.getCuisin().getId()
								, lunch.getDessert().getId());
	}

	public static Lunch toEntity(LunchDto lunchDTO, Cuisin cuisin, Dessert dessert) {
		return new Lunch(lunchDTO.getId()
							, cuisin
							, dessert);
	}

}
