package io.gitlab.ciskos.foodorderingsystem.converter;

import io.gitlab.ciskos.foodorderingsystem.dto.DessertDto;
import io.gitlab.ciskos.foodorderingsystem.model.meal.dessert.Dessert;

public class ConvertDessert {
	
	public static DessertDto toDto(Dessert dessert) {
		return new DessertDto(dessert.getId()
								, dessert.getName()
								, dessert.getPrice());
	}

	public static Dessert toEntity(DessertDto dessertDTO) {
		return new Dessert(dessertDTO.getId()
							, dessertDTO.getName()
							, dessertDTO.getPrice());
	}

}
