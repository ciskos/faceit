package io.gitlab.ciskos.foodorderingsystem.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.foodorderingsystem.converter.ConvertDessert;
import io.gitlab.ciskos.foodorderingsystem.dto.DessertDto;
import io.gitlab.ciskos.foodorderingsystem.service.DessertService;

@RestController
@RequestMapping(value = DessertRestController.DESSERT_PATH, produces = "application/json")
public class DessertRestController {
	
	static final String DESSERT_PATH = "/desserts";

	private DessertService dessertService;

	public DessertRestController(DessertService dessertService) {
		this.dessertService = dessertService;
	}

	@GetMapping
	public List<DessertDto> getAllDesserts() {
		var allDesserts = dessertService.getAllDesserts();
		
		return allDesserts.stream()
						.map(a -> ConvertDessert.toDto(a))
						.collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<DessertDto> getDessertById(@PathVariable("id") Long id) {
		var dessert = dessertService.getDessertById(id);
		
		if (dessert.getId() != null) {
			return new ResponseEntity<>(ConvertDessert.toDto(dessert), HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public DessertDto addDessert(@RequestBody DessertDto dessertDTO) {
		var dessert = ConvertDessert.toEntity(dessertDTO);
		
		return ConvertDessert.toDto(dessertService.addDessert(dessert));
	}
	
	@PutMapping
	public DessertDto putDessert(@RequestBody DessertDto dessertDTO) {
		var dessert = ConvertDessert.toEntity(dessertDTO);
		
		return ConvertDessert.toDto(dessertService.addDessert(dessert));
	}

	@PatchMapping(path = "/{dessertId}", consumes = "application/json")
	public DessertDto patchDessert(
			@PathVariable("dessertId") Long dessertId
			, @RequestBody DessertDto dessertDTO) {
		var dessert = dessertService.getDessertById(dessertId);
		
		if (dessertDTO.getName() != null) dessert.setName(dessertDTO.getName());
		if (dessertDTO.getPrice() != null) dessert.setPrice(dessertDTO.getPrice());
		
		return ConvertDessert.toDto(dessertService.addDessert(dessert));
	}

	@DeleteMapping("/{dessertId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteDessert(@PathVariable("dessertId") Long dessertId) {
		try {
			dessertService.deleteDessertById(dessertId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
