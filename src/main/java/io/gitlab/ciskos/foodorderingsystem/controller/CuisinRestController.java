package io.gitlab.ciskos.foodorderingsystem.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.foodorderingsystem.converter.ConvertCuisin;
import io.gitlab.ciskos.foodorderingsystem.dto.CuisinDto;
import io.gitlab.ciskos.foodorderingsystem.service.CuisinService;

@RestController
@RequestMapping(value = CuisinRestController.CUISIN_PATH, produces = "application/json")
public class CuisinRestController {
	
	static final String CUISIN_PATH = "/cuisins";

	private CuisinService cuisinService;

	public CuisinRestController(CuisinService cuisinService) {
		this.cuisinService = cuisinService;
	}

	@GetMapping
	public List<CuisinDto> getAllCuisins() {
		var allCuisins = cuisinService.getAllCuisins();
		
		return allCuisins.stream()
						.map(a -> ConvertCuisin.toDto(a))
						.collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<CuisinDto> getCuisinById(@PathVariable("id") Long id) {
		var cuisin = cuisinService.getCuisinById(id);
		
		if (cuisin.getId() != null) {
			return new ResponseEntity<>(ConvertCuisin.toDto(cuisin), HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public CuisinDto addCuisin(@RequestBody CuisinDto cuisinDTO) {
		var cuisin = ConvertCuisin.toEntity(cuisinDTO);
		
		return ConvertCuisin.toDto(cuisinService.addCuisin(cuisin));
	}
	
	@PutMapping
	public CuisinDto putCuisin(@RequestBody CuisinDto cuisinDTO) {
		var cuisin = ConvertCuisin.toEntity(cuisinDTO);
		
		return ConvertCuisin.toDto(cuisinService.addCuisin(cuisin));
	}

	@PatchMapping(path = "/{cuisinId}", consumes = "application/json")
	public CuisinDto patchCuisin(
			@PathVariable("cuisinId") Long cuisinId
			, @RequestBody CuisinDto cuisinDTO) {
		var cuisin = cuisinService.getCuisinById(cuisinId);
		
		if (cuisinDTO.getCuisin() != null) cuisin.setCuisin(cuisinDTO.getCuisin());
		if (cuisinDTO.getDish() != null) cuisin.setDish(cuisinDTO.getDish());
		if (cuisinDTO.getPrice() != null) cuisin.setPrice(cuisinDTO.getPrice());

		return ConvertCuisin.toDto(cuisinService.addCuisin(cuisin));
	}

	@DeleteMapping("/{cuisinId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteCuisin(@PathVariable("cuisinId") Long cuisinId) {
		try {
			cuisinService.deleteCuisinById(cuisinId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
