package io.gitlab.ciskos.foodorderingsystem.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.foodorderingsystem.converter.ConvertLunch;
import io.gitlab.ciskos.foodorderingsystem.dto.LunchDto;
import io.gitlab.ciskos.foodorderingsystem.service.CuisinService;
import io.gitlab.ciskos.foodorderingsystem.service.DessertService;
import io.gitlab.ciskos.foodorderingsystem.service.LunchService;

@RestController
@RequestMapping(value = LunchRestController.LUNCH_PATH, produces = "application/json")
public class LunchRestController {
	
	static final String LUNCH_PATH = "/lunchs";

	private LunchService lunchService;
	private CuisinService cuisinService;
	private DessertService dessertService;

	public LunchRestController(LunchService lunchService, CuisinService cuisinService, DessertService dessertService) {
		this.lunchService = lunchService;
		this.cuisinService = cuisinService;
		this.dessertService = dessertService;
	}

	@GetMapping
	public List<LunchDto> getAllLunchs() {
		var allLunchs = lunchService.getAllLunchs();
		
		return allLunchs.stream()
						.map(a -> ConvertLunch.toDto(a))
						.collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<LunchDto> getLunchById(@PathVariable("id") Long id) {
		var lunch = lunchService.getLunchById(id);
		
		if (lunch.getId() != null) {
			return new ResponseEntity<>(ConvertLunch.toDto(lunch), HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public LunchDto addLunch(@RequestBody LunchDto lunchDTO) {
		var cuisin = cuisinService.getCuisinById(lunchDTO.getCuisinId());
		var dessert = dessertService.getDessertById(lunchDTO.getDessertId());
		var lunch = ConvertLunch.toEntity(lunchDTO, cuisin, dessert);
		
		return ConvertLunch.toDto(lunchService.addLunch(lunch));
	}
	
	@PutMapping
	public LunchDto putLunch(@RequestBody LunchDto lunchDTO) {
		var cuisin = cuisinService.getCuisinById(lunchDTO.getCuisinId());
		var dessert = dessertService.getDessertById(lunchDTO.getDessertId());
		var lunch = ConvertLunch.toEntity(lunchDTO, cuisin, dessert);
		
		return ConvertLunch.toDto(lunchService.addLunch(lunch));
	}

	@PatchMapping(path = "/{lunchId}", consumes = "application/json")
	public LunchDto patchLunch(
			@PathVariable("lunchId") Long lunchId
			, @RequestBody LunchDto lunchDTO) {
		var lunch = lunchService.getLunchById(lunchId);
		
		if (lunchDTO.getCuisinId() != null) {
			var cuisin = cuisinService.getCuisinById(lunchDTO.getCuisinId());
			lunch.setCuisin(cuisin);
		}
		
		if (lunchDTO.getDessertId() != null) {
			var dessert = dessertService.getDessertById(lunchDTO.getDessertId());
			lunch.setDessert(dessert);
		}
		
		return ConvertLunch.toDto(lunchService.addLunch(lunch));
	}

	@DeleteMapping("/{lunchId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteLunch(@PathVariable("lunchId") Long lunchId) {
		try {
			lunchService.deleteLunchById(lunchId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
