package io.gitlab.ciskos.foodorderingsystem.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.foodorderingsystem.converter.ConvertOrder;
import io.gitlab.ciskos.foodorderingsystem.dto.OrderDto;
import io.gitlab.ciskos.foodorderingsystem.service.DrinkService;
import io.gitlab.ciskos.foodorderingsystem.service.LunchService;
import io.gitlab.ciskos.foodorderingsystem.service.OrderService;

@RestController
@RequestMapping(value = OrderRestController.ORDER_PATH, produces = "application/json")
public class OrderRestController {
	
	static final String ORDER_PATH = "/orders";

	private OrderService orderService;
	private LunchService lunchService;
	private DrinkService drinkService;

	public OrderRestController(OrderService orderService, LunchService lunchService, DrinkService drinkService) {
		this.orderService = orderService;
		this.lunchService = lunchService;
		this.drinkService = drinkService;
	}

	@GetMapping
	public List<OrderDto> getAllOrders() {
		var allOrders = orderService.getAllOrders();
		
		return allOrders.stream()
						.map(a -> ConvertOrder.toDto(a))
						.collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<OrderDto> getOrderById(@PathVariable("id") Long id) {
		var order = orderService.getOrderById(id);
		
		if (order.getId() != null) {
			return new ResponseEntity<>(ConvertOrder.toDto(order), HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public OrderDto addOrder(@RequestBody OrderDto orderDTO) {
		var lunch = lunchService.getLunchById(orderDTO.getLunchId());
		var drink = drinkService.getDrinkById(orderDTO.getDrinkId());
		
		Double totalPrice = lunch.getCuisin().getPrice()
				+ lunch.getDessert().getPrice()
				+ drink.getPrice();
		
		orderDTO.setTotalPrice(totalPrice);

		var order = ConvertOrder.toEntity(orderDTO, lunch, drink);
		
		return ConvertOrder.toDto(orderService.addOrder(order));
	}
	
	@PutMapping
	public OrderDto putOrder(@RequestBody OrderDto orderDTO) {
		var lunch = lunchService.getLunchById(orderDTO.getLunchId());
		var drink = drinkService.getDrinkById(orderDTO.getDrinkId());
		var order = ConvertOrder.toEntity(orderDTO, lunch, drink);
		
		return ConvertOrder.toDto(orderService.addOrder(order));
	}

	@PatchMapping(path = "/{orderId}", consumes = "application/json")
	public OrderDto patchOrder(
			@PathVariable("orderId") Long orderId
			, @RequestBody OrderDto orderDTO) {
		var order = orderService.getOrderById(orderId);
		
		if (orderDTO.getLunchId() != null) {
			var lunch = lunchService.getLunchById(orderDTO.getLunchId());
			order.setLunch(lunch);
		}
		
		if (orderDTO.getDrinkId() != null) {
			var drink = drinkService.getDrinkById(orderDTO.getDrinkId());
			order.setDrink(drink);
		}
		
		if (orderDTO.getIce() != null) {
			order.setIce(orderDTO.getIce());
		}
		
		if (orderDTO.getLemon() != null) {
			order.setLemon(orderDTO.getLemon());
		}
		
		if (orderDTO.getTotalPrice() != null) {
			order.setTotalPrice(orderDTO.getTotalPrice());
		}
		
		return ConvertOrder.toDto(orderService.addOrder(order));
	}

	@DeleteMapping("/{orderId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteOrder(@PathVariable("orderId") Long orderId) {
		try {
			orderService.deleteOrderById(orderId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
