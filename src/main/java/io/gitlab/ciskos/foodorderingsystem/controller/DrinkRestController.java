package io.gitlab.ciskos.foodorderingsystem.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.ciskos.foodorderingsystem.converter.ConvertDrink;
import io.gitlab.ciskos.foodorderingsystem.dto.DrinkDto;
import io.gitlab.ciskos.foodorderingsystem.service.DrinkService;

@RestController
@RequestMapping(value = DrinkRestController.DRINK_PATH, produces = "application/json")
public class DrinkRestController {
	
	static final String DRINK_PATH = "/drinks";

	private DrinkService drinkService;

	public DrinkRestController(DrinkService drinkService) {
		this.drinkService = drinkService;
	}

	@GetMapping
	public List<DrinkDto> getAllDrinks() {
		var allDrinks = drinkService.getAllDrinks();
		
		return allDrinks.stream()
						.map(a -> ConvertDrink.toDto(a))
						.collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<DrinkDto> getDrinkById(@PathVariable("id") Long id) {
		var drink = drinkService.getDrinkById(id);
		
		if (drink.getId() != null) {
			return new ResponseEntity<>(ConvertDrink.toDto(drink), HttpStatus.OK);
		}
		
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public DrinkDto addDrink(@RequestBody DrinkDto drinkDTO) {
		var drink = ConvertDrink.toEntity(drinkDTO);
		
		return ConvertDrink.toDto(drinkService.addDrink(drink));
	}
	
	@PutMapping
	public DrinkDto putDrink(@RequestBody DrinkDto drinkDTO) {
		var drink = ConvertDrink.toEntity(drinkDTO);
		
		return ConvertDrink.toDto(drinkService.addDrink(drink));
	}

	@PatchMapping(path = "/{drinkId}", consumes = "application/json")
	public DrinkDto patchDrink(
			@PathVariable("drinkId") Long drinkId
			, @RequestBody DrinkDto drinkDTO) {
		var drink = drinkService.getDrinkById(drinkId);
		
		if (drinkDTO.getName() != null) drink.setName(drinkDTO.getName());
		if (drinkDTO.getPrice() != null) drink.setPrice(drinkDTO.getPrice());
		
		return ConvertDrink.toDto(drinkService.addDrink(drink));
	}

	@DeleteMapping("/{drinkId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteDrink(@PathVariable("drinkId") Long drinkId) {
		try {
			drinkService.deleteDrinkById(drinkId);
		} catch (EmptyResultDataAccessException e) {}
	}

}
