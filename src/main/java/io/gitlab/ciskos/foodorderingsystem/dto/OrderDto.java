package io.gitlab.ciskos.foodorderingsystem.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
	
	private Long id;

	@NotNull(message = "Lunch id must not be null.")
	private Long lunchId;
	
	@NotNull(message = "Drink id must not be null.")
	private Long drinkId;
	
	private Boolean ice;
	
	private Boolean lemon;
	
	@NotNull(message = "Price must not be null.")
	private Double totalPrice;

}
