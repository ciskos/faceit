package io.gitlab.ciskos.foodorderingsystem.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LunchDto {
	
	private Long id;
	
	@NotNull(message = "Cuisin id must not be null.")
	private Long cuisinId;
	
	@NotNull(message = "Dessert id must not be null.")
	private Long dessertId;

}
