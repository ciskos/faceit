package io.gitlab.ciskos.foodorderingsystem.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DessertDto {
	
	private Long id;
	
	@NotBlank(message = "Name must not be blank.")
	private String name;
	
	@NotNull(message = "Price must not be null.")
	private Double price;

}
