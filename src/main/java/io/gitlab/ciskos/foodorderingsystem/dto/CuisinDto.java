package io.gitlab.ciskos.foodorderingsystem.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CuisinDto {

	private Long id;
	
	@NotBlank(message = "Cuisin must not be blank.")
	private String cuisin;
	
	@NotBlank(message = "Dish must not be blank.")
	private String dish;
	
	@NotNull(message = "Price must not be null.")
	private Double price;
	
}
