package io.gitlab.ciskos.foodorderingsystem.service;

import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import io.gitlab.ciskos.foodorderingsystem.model.meal.Lunch;
import io.gitlab.ciskos.foodorderingsystem.repository.LunchRepository;

@Service
public class LunchService {

	private LunchRepository lunchRepository;

	public LunchService(LunchRepository lunchRepository) {
		this.lunchRepository = lunchRepository;
	}

	public List<Lunch> getAllLunchs() {
		return StreamSupport.stream(lunchRepository.findAll().spliterator(), false).toList();
	}

	public Lunch addLunch(Lunch lunch) {
		return lunchRepository.save(lunch);
	}

	public Lunch getLunchById(Long lunchId) {
		return lunchRepository.findById(lunchId).orElse(new Lunch());
	}

	public Lunch updateLunch(Lunch lunch) {
		return lunchRepository.save(lunch);
	}

	public void deleteLunchById(Long lunchId) {
		lunchRepository.deleteById(lunchId);
	}

}
