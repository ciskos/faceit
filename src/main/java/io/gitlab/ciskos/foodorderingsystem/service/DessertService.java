package io.gitlab.ciskos.foodorderingsystem.service;

import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import io.gitlab.ciskos.foodorderingsystem.model.meal.dessert.Dessert;
import io.gitlab.ciskos.foodorderingsystem.repository.DessertRepository;

@Service
public class DessertService {

	private DessertRepository dessertRepository;

	public DessertService(DessertRepository dessertRepository) {
		this.dessertRepository = dessertRepository;
	}

	public List<Dessert> getAllDesserts() {
		return StreamSupport.stream(dessertRepository.findAll().spliterator(), false).toList();
	}

	public Dessert addDessert(Dessert dessert) {
		return dessertRepository.save(dessert);
	}

	public Dessert getDessertById(Long dessertId) {
		return dessertRepository.findById(dessertId).orElse(new Dessert());
	}

	public Dessert updateDessert(Dessert dessert) {
		return dessertRepository.save(dessert);
	}

	public void deleteDessertById(Long dessertId) {
		dessertRepository.deleteById(dessertId);
	}

}
