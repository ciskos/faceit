package io.gitlab.ciskos.foodorderingsystem.service;

import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import io.gitlab.ciskos.foodorderingsystem.model.order.Order;
import io.gitlab.ciskos.foodorderingsystem.repository.OrderRepository;

@Service
public class OrderService {

	private OrderRepository orderRepository;

	public OrderService(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	public List<Order> getAllOrders() {
		return StreamSupport.stream(orderRepository.findAll().spliterator(), false).toList();
	}

	public Order addOrder(Order order) {
		return orderRepository.save(order);
	}

	public Order getOrderById(Long orderId) {
		return orderRepository.findById(orderId).orElse(new Order());
	}

	public Order updateOrder(Order order) {
		return orderRepository.save(order);
	}

	public void deleteOrderById(Long orderId) {
		orderRepository.deleteById(orderId);
	}

}
