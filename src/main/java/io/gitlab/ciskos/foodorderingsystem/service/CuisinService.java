package io.gitlab.ciskos.foodorderingsystem.service;

import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import io.gitlab.ciskos.foodorderingsystem.model.meal.course.Cuisin;
import io.gitlab.ciskos.foodorderingsystem.repository.CuisinRepository;

@Service
public class CuisinService {

	private CuisinRepository cuisinRepository;

	public CuisinService(CuisinRepository cuisinRepository) {
		this.cuisinRepository = cuisinRepository;
	}

	public List<Cuisin> getAllCuisins() {
		return StreamSupport.stream(cuisinRepository.findAll().spliterator(), false).toList();
	}

	public Cuisin addCuisin(Cuisin cuisin) {
		return cuisinRepository.save(cuisin);
	}

	public Cuisin getCuisinById(Long cuisinId) {
		return cuisinRepository.findById(cuisinId).orElse(new Cuisin());
	}

	public Cuisin updateCuisin(Cuisin cuisin) {
		return cuisinRepository.save(cuisin);
	}

	public void deleteCuisinById(Long cuisinId) {
		cuisinRepository.deleteById(cuisinId);
	}

}
