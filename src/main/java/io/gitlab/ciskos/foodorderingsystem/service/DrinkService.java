package io.gitlab.ciskos.foodorderingsystem.service;

import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import io.gitlab.ciskos.foodorderingsystem.model.drink.Drink;
import io.gitlab.ciskos.foodorderingsystem.repository.DrinkRepository;

@Service
public class DrinkService {

	private DrinkRepository drinkRepository;

	public DrinkService(DrinkRepository drinkRepository) {
		this.drinkRepository = drinkRepository;
	}

	public List<Drink> getAllDrinks() {
		return StreamSupport.stream(drinkRepository.findAll().spliterator(), false).toList();
	}

	public Drink addDrink(Drink drink) {
		return drinkRepository.save(drink);
	}

	public Drink getDrinkById(Long drinkId) {
		return drinkRepository.findById(drinkId).orElse(new Drink());
	}

	public Drink updateDrink(Drink drink) {
		return drinkRepository.save(drink);
	}

	public void deleteDrinkById(Long drinkId) {
		drinkRepository.deleteById(drinkId);
	}

}
