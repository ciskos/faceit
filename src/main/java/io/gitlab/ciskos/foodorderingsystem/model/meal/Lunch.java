package io.gitlab.ciskos.foodorderingsystem.model.meal;

import io.gitlab.ciskos.foodorderingsystem.model.meal.course.Cuisin;
import io.gitlab.ciskos.foodorderingsystem.model.meal.dessert.Dessert;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "LUNCHS")
public class Lunch extends AbstractLunch {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@NotNull
	@JoinColumn(name = "CUISIN_ID", nullable = false)
	@ManyToOne(cascade = CascadeType.ALL)
	private Cuisin cuisin;
	
	@NotNull
	@JoinColumn(name = "DESSERT_ID", nullable = false)
	@ManyToOne(cascade = CascadeType.ALL)
	private Dessert dessert;
	
}
