package io.gitlab.ciskos.foodorderingsystem.model.order;

import io.gitlab.ciskos.foodorderingsystem.model.drink.Drink;
import io.gitlab.ciskos.foodorderingsystem.model.meal.Lunch;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ORDERS")
public class Order extends AbstractOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@NotNull
	@JoinColumn(name = "LUNCH_ID", nullable = false)
	@ManyToOne(cascade = CascadeType.ALL)
	private Lunch lunch;
	
	@NotNull
	@JoinColumn(name = "DRINK_ID", nullable = false)
	@ManyToOne(cascade = CascadeType.ALL)
	private Drink drink;
	
	@Column(name = "ICE")
	private Boolean ice;
	
	@Column(name = "LEMON")
	private Boolean lemon;
	
	@NotNull
	@Column(name = "TOTAL_PRICE")
	private Double totalPrice;
	
}
