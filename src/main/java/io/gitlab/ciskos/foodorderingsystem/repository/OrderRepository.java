package io.gitlab.ciskos.foodorderingsystem.repository;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.ciskos.foodorderingsystem.model.order.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {

}
