package io.gitlab.ciskos.foodorderingsystem.repository;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.ciskos.foodorderingsystem.model.meal.Lunch;

public interface LunchRepository extends CrudRepository<Lunch, Long> {

}
