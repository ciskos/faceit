package io.gitlab.ciskos.foodorderingsystem.repository;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.ciskos.foodorderingsystem.model.drink.Drink;

public interface DrinkRepository extends CrudRepository<Drink, Long> {

}
