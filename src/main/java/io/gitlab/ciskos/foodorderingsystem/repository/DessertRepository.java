package io.gitlab.ciskos.foodorderingsystem.repository;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.ciskos.foodorderingsystem.model.meal.dessert.Dessert;

public interface DessertRepository extends CrudRepository<Dessert, Long> {

}
