package io.gitlab.ciskos.foodorderingsystem.repository;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.ciskos.foodorderingsystem.model.meal.course.Cuisin;

public interface CuisinRepository extends CrudRepository<Cuisin, Long> {

}
