INSERT INTO DRINKS(name, price) VALUES('DRINK1', 100.0);
INSERT INTO DRINKS(name, price) VALUES('DRINK2', 100.0);
INSERT INTO DRINKS(name, price) VALUES('DRINK3', 100.0);

INSERT INTO DESSERTS(name, price) VALUES('Dessert1', 100.0);
INSERT INTO DESSERTS(name, price) VALUES('Dessert2', 100.0);
INSERT INTO DESSERTS(name, price) VALUES('Dessert3', 100.0);

INSERT INTO CUISINS(cuisin, dish, price) VALUES('Polish', 'PolishDish1', 200.0);
INSERT INTO CUISINS(cuisin, dish, price) VALUES('Polish', 'PolishDish2', 200.0);
INSERT INTO CUISINS(cuisin, dish, price) VALUES('Polish', 'PolishDish3', 200.0);
INSERT INTO CUISINS(cuisin, dish, price) VALUES('Mexican', 'MexixcanDish1', 200.0);
INSERT INTO CUISINS(cuisin, dish, price) VALUES('Mexican', 'MexixcanDish2', 200.0);
INSERT INTO CUISINS(cuisin, dish, price) VALUES('Mexican', 'MexixcanDish3', 200.0);
INSERT INTO CUISINS(cuisin, dish, price) VALUES('Italian', 'ItalianDish1', 200.0);
INSERT INTO CUISINS(cuisin, dish, price) VALUES('Italian', 'ItalianDish2', 200.0);
INSERT INTO CUISINS(cuisin, dish, price) VALUES('Italian', 'ItalianDish3', 200.0);

INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(1, 1);
INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(2, 2);
INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(3, 3);
INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(4, 1);
INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(5, 2);
INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(6, 3);
INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(7, 1);
INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(8, 2);
INSERT INTO LUNCHS(cuisin_id, dessert_id) VALUES(9, 3);

INSERT INTO ORDERS(lunch_id, drink_id, ice, lemon, total_price) VALUES(1, 1, true, true, 400.0);
INSERT INTO ORDERS(lunch_id, drink_id, ice, lemon, total_price) VALUES(1, 1, true, true, 400.0);
INSERT INTO ORDERS(lunch_id, drink_id, ice, lemon, total_price) VALUES(1, 1, true, true, 400.0);
