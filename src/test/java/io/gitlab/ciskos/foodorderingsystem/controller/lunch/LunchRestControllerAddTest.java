package io.gitlab.ciskos.foodorderingsystem.controller.lunch;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class LunchRestControllerAddTest extends AbstractLunchRestControllerTest {

	@Test
	@DisplayName("Test add lunch")
	@DirtiesContext
	void testAddLunch() throws Exception {
		var content = "{\"id\":null,\"cuisinId\":1,\"dessertId\":1}";
		var result = "{\"id\":10,\"cuisinId\":1,\"dessertId\":1}";
		
		mockMvc.perform(post("/lunchs")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
