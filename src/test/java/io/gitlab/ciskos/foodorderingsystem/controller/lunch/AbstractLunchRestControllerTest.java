package io.gitlab.ciskos.foodorderingsystem.controller.lunch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class AbstractLunchRestControllerTest {

	protected static final Long TEST_LUNCH_ID = 1L;
	
	@Autowired
	protected MockMvc mockMvc;

}