package io.gitlab.ciskos.foodorderingsystem.controller.cuisin;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class CuisinRestControllerUpdateTest extends AbstractCuisinRestControllerTest {
	
	@Test
	@DisplayName("Test cuisin put")
	@DirtiesContext
	void testPutCuisin() throws Exception {
		var content = "{\"id\":1,\"cuisin\":\"UpdatedMexicanCuisin\",\"dish\":\"UpdatedMexicanDish\",\"price\":500.0}";
		var result = "{\"id\":1,\"cuisin\":\"UpdatedMexicanCuisin\",\"dish\":\"UpdatedMexicanDish\",\"price\":500.0}";

		mockMvc.perform(put("/cuisins")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	@DisplayName("Test cuisin patch")
	@DirtiesContext
	void testPatchCuisin() throws Exception {
		var content = "{\"dish\":\"UpdatedMexicanDish\",\"price\":1000.0}";
		var result = "{\"id\":1,\"cuisin\":\"Polish\",\"dish\":\"UpdatedMexicanDish\",\"price\":1000.0}";

		mockMvc.perform(patch("/cuisins/{id}", TEST_CUISIN_ID)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
