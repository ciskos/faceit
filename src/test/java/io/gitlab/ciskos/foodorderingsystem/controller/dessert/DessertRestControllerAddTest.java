package io.gitlab.ciskos.foodorderingsystem.controller.dessert;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class DessertRestControllerAddTest extends AbstractDessertRestControllerTest {

	@Test
	@DisplayName("Test add dessert")
	@DirtiesContext
	void testAddDessert() throws Exception {
		var content = "{\"id\":null,\"name\":\"NewDessert\",\"price\":300.0}";
		var result = "{\"id\":4,\"name\":\"NewDessert\",\"price\":300.0}";
		
		mockMvc.perform(post("/desserts")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
