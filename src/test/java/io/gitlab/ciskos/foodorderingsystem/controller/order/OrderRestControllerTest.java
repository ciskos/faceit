package io.gitlab.ciskos.foodorderingsystem.controller.order;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class OrderRestControllerTest extends AbstractOrderRestControllerTest {

	@Test
	@DisplayName("Test get all orders")
	void testGetAllOrders() throws Exception {
		var result = "[{\"id\":1,\"lunchId\":1,\"drinkId\":1,\"ice\":true,\"lemon\":true,\"totalPrice\":400.0},{\"id\":2,\"lunchId\":1,\"drinkId\":1,\"ice\":true,\"lemon\":true,\"totalPrice\":400.0},{\"id\":3,\"lunchId\":1,\"drinkId\":1,\"ice\":true,\"lemon\":true,\"totalPrice\":400.0}]";
		
		mockMvc.perform(get("/orders")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test get order by id")
	void testGetOrderById() throws Exception {
		var result = "{\"id\":1,\"lunchId\":1,\"drinkId\":1,\"ice\":true,\"lemon\":true,\"totalPrice\":400.0}";
		
		mockMvc.perform(get("/orders/{id}", TEST_ORDER_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test order delete")
	@DirtiesContext
	void testDeleteOrder() throws Exception {
		mockMvc.perform(delete("/orders/{id}", TEST_ORDER_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful());
	}

}
