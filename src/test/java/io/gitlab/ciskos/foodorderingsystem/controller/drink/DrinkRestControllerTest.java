package io.gitlab.ciskos.foodorderingsystem.controller.drink;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class DrinkRestControllerTest extends AbstractDrinkRestControllerTest {

	@Test
	@DisplayName("Test get all drinks")
	void testGetAllDrinks() throws Exception {
		var result = "[{\"id\":1,\"name\":\"DRINK1\",\"price\":100.0},{\"id\":2,\"name\":\"DRINK2\",\"price\":100.0},{\"id\":3,\"name\":\"DRINK3\",\"price\":100.0}]";
		
		mockMvc.perform(get("/drinks")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test get drink by id")
	void testGetDrinkById() throws Exception {
		var result = "{\"id\":1,\"name\":\"DRINK1\",\"price\":100.0}";
		
		mockMvc.perform(get("/drinks/{id}", TEST_DRINK_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test drink delete")
	@DirtiesContext
	void testDeleteDrink() throws Exception {
		mockMvc.perform(delete("/drinks/{id}", TEST_DRINK_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful());
	}

}
