package io.gitlab.ciskos.foodorderingsystem.controller.order;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class OrderRestControllerUpdateTest extends AbstractOrderRestControllerTest {
	
	@Test
	@DisplayName("Test order put")
	@DirtiesContext
	void testPutOrder() throws Exception {
		var content = "{\"id\":1,\"lunchId\":1,\"drinkId\":1,\"ice\":\"false\",\"lemon\":\"true\",\"totalPrice\":400.0}";
		var result = "{\"id\":1,\"lunchId\":1,\"drinkId\":1,\"ice\":false,\"lemon\":true,\"totalPrice\":400.0}";

		mockMvc.perform(put("/orders")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	@DisplayName("Test order patch")
	@DirtiesContext
	void testPatchOrder() throws Exception {
		var content = "{\"ice\":false,\"lemon\":false,\"totalPrice\":1000.0}";
		var result = "{\"id\":1,\"lunchId\":1,\"drinkId\":1,\"ice\":false,\"lemon\":false,\"totalPrice\":1000.0}";

		mockMvc.perform(patch("/orders/{id}", TEST_ORDER_ID)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
