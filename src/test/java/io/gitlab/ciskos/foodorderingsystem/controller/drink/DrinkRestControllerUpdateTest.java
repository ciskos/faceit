package io.gitlab.ciskos.foodorderingsystem.controller.drink;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class DrinkRestControllerUpdateTest extends AbstractDrinkRestControllerTest {
	
	@Test
	@DisplayName("Test drink put")
	@DirtiesContext
	void testPutDrink() throws Exception {
		var content = "{\"id\":1,\"name\":\"UpdatedDrink\",\"price\":500.0}";
		var result = "{\"id\":1,\"name\":\"UpdatedDrink\",\"price\":500.0}";

		mockMvc.perform(put("/drinks")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	@DisplayName("Test drink patch")
	@DirtiesContext
	void testPatchDrink() throws Exception {
		var content = "{\"name\":\"DrinkPatched\",\"price\":1000.0}";
		var result = "{\"id\":1,\"name\":\"DrinkPatched\",\"price\":1000.0}";

		mockMvc.perform(patch("/drinks/{id}", TEST_DRINK_ID)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
