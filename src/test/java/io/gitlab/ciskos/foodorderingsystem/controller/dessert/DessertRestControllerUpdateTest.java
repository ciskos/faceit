package io.gitlab.ciskos.foodorderingsystem.controller.dessert;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class DessertRestControllerUpdateTest extends AbstractDessertRestControllerTest {
	
	@Test
	@DisplayName("Test dessert put")
	@DirtiesContext
	void testPutDessert() throws Exception {
		var content = "{\"id\":1,\"name\":\"UpdatedDessert\",\"price\":500.0}";
		var result = "{\"id\":1,\"name\":\"UpdatedDessert\",\"price\":500.0}";

		mockMvc.perform(put("/desserts")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	@DisplayName("Test dessert patch")
	@DirtiesContext
	void testPatchDessert() throws Exception {
		var content = "{\"name\":\"DessertPatched\",\"price\":1000.0}";
		var result = "{\"id\":1,\"name\":\"DessertPatched\",\"price\":1000.0}";

		mockMvc.perform(patch("/desserts/{id}", TEST_DESSERT_ID)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
