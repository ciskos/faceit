package io.gitlab.ciskos.foodorderingsystem.controller.lunch;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class LunchRestControllerUpdateTest extends AbstractLunchRestControllerTest {
	
	@Test
	@DisplayName("Test lunch put")
	@DirtiesContext
	void testPutLunch() throws Exception {
		var content = "{\"id\":1,\"cuisinId\":1,\"dessertId\":1}";
		var result = "{\"id\":1,\"cuisinId\":1,\"dessertId\":1}";

		mockMvc.perform(put("/lunchs")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}
	
	@Test
	@DisplayName("Test lunch patch")
	@DirtiesContext
	void testPatchLunch() throws Exception {
		var content = "{\"dessertId\":3}";
		var result = "{\"id\":1,\"cuisinId\":1,\"dessertId\":3}";

		mockMvc.perform(patch("/lunchs/{id}", TEST_LUNCH_ID)
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

}
