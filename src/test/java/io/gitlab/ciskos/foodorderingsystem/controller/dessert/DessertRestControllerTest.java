package io.gitlab.ciskos.foodorderingsystem.controller.dessert;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class DessertRestControllerTest extends AbstractDessertRestControllerTest {

	@Test
	@DisplayName("Test get all desserts")
	void testGetAllDesserts() throws Exception {
		var result = "[{\"id\":1,\"name\":\"Dessert1\",\"price\":100.0},{\"id\":2,\"name\":\"Dessert2\",\"price\":100.0},{\"id\":3,\"name\":\"Dessert3\",\"price\":100.0}]";
		
		mockMvc.perform(get("/desserts")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test get dessert by id")
	void testGetDessertById() throws Exception {
		var result = "{\"id\":1,\"name\":\"Dessert1\",\"price\":100.0}";
		
		mockMvc.perform(get("/desserts/{id}", TEST_DESSERT_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test dessert delete")
	@DirtiesContext
	void testDeleteDessert() throws Exception {
		mockMvc.perform(delete("/desserts/{id}", TEST_DESSERT_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful());
	}

}
