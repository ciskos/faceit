package io.gitlab.ciskos.foodorderingsystem.controller.order;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class OrderRestControllerAddTest extends AbstractOrderRestControllerTest {

	@Test
	@DisplayName("Test add order")
	@DirtiesContext
	void testAddOrder() throws Exception {
		var content = "{\"id\":null,\"lunchId\":1,\"drinkId\":1,\"ice\":\"true\",\"lemon\":\"true\",\"totalPrice\":0.0}";
		var result = "{\"id\":4,\"lunchId\":1,\"drinkId\":1,\"ice\":true,\"lemon\":true,\"totalPrice\":400.0}";
		
		mockMvc.perform(post("/orders")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
