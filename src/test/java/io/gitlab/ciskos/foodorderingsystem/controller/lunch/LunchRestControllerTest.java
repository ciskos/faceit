package io.gitlab.ciskos.foodorderingsystem.controller.lunch;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class LunchRestControllerTest extends AbstractLunchRestControllerTest {

	@Test
	@DisplayName("Test get all lunchs")
	void testGetAllLunchs() throws Exception {
		var result = "[{\"id\":1,\"cuisinId\":1,\"dessertId\":1},{\"id\":2,\"cuisinId\":2,\"dessertId\":2},{\"id\":3,\"cuisinId\":3,\"dessertId\":3},{\"id\":4,\"cuisinId\":4,\"dessertId\":1},{\"id\":5,\"cuisinId\":5,\"dessertId\":2},{\"id\":6,\"cuisinId\":6,\"dessertId\":3},{\"id\":7,\"cuisinId\":7,\"dessertId\":1},{\"id\":8,\"cuisinId\":8,\"dessertId\":2},{\"id\":9,\"cuisinId\":9,\"dessertId\":3}]";
		
		mockMvc.perform(get("/lunchs")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test get lunch by id")
	void testGetLunchById() throws Exception {
		var result = "{\"id\":1,\"cuisinId\":1,\"dessertId\":1}";
		
		mockMvc.perform(get("/lunchs/{id}", TEST_LUNCH_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test lunch delete")
	@DirtiesContext
	void testDeleteLunch() throws Exception {
		mockMvc.perform(delete("/lunchs/{id}", TEST_LUNCH_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful());
	}

}
