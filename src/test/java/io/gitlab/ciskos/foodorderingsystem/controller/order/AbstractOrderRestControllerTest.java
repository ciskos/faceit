package io.gitlab.ciskos.foodorderingsystem.controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class AbstractOrderRestControllerTest {

	protected static final Long TEST_ORDER_ID = 1L;
	
	@Autowired
	protected MockMvc mockMvc;

}