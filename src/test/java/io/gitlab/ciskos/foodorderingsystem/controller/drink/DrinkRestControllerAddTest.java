package io.gitlab.ciskos.foodorderingsystem.controller.drink;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class DrinkRestControllerAddTest extends AbstractDrinkRestControllerTest {

	@Test
	@DisplayName("Test add drink")
	@DirtiesContext
	void testAddDrink() throws Exception {
		var content = "{\"id\":null,\"name\":\"NewDrink\",\"price\":300.0}";
		var result = "{\"id\":4,\"name\":\"NewDrink\",\"price\":300.0}";
		
		mockMvc.perform(post("/drinks")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
