package io.gitlab.ciskos.foodorderingsystem.controller.cuisin;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class CuisinRestControllerAddTest extends AbstractCuisinRestControllerTest {

	@Test
	@DisplayName("Test add cuisin")
	@DirtiesContext
	void testAddCuisin() throws Exception {
		var content = "{\"id\":null,\"cuisin\":\"Polish\",\"dish\":\"PolishDishNew\",\"price\":300.0}";
		var result = "{\"id\":10,\"cuisin\":\"Polish\",\"dish\":\"PolishDishNew\",\"price\":300.0}";
		
		mockMvc.perform(post("/cuisins")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
