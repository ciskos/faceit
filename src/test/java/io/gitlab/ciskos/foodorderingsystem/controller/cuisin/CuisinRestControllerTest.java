package io.gitlab.ciskos.foodorderingsystem.controller.cuisin;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

class CuisinRestControllerTest extends AbstractCuisinRestControllerTest {

	@Test
	@DisplayName("Test get all cuisins")
	void testGetAllCuisins() throws Exception {
		var result = "[{\"id\":1,\"cuisin\":\"Polish\",\"dish\":\"PolishDish1\",\"price\":200.0},{\"id\":2,\"cuisin\":\"Polish\",\"dish\":\"PolishDish2\",\"price\":200.0},{\"id\":3,\"cuisin\":\"Polish\",\"dish\":\"PolishDish3\",\"price\":200.0},{\"id\":4,\"cuisin\":\"Mexican\",\"dish\":\"MexixcanDish1\",\"price\":200.0},{\"id\":5,\"cuisin\":\"Mexican\",\"dish\":\"MexixcanDish2\",\"price\":200.0},{\"id\":6,\"cuisin\":\"Mexican\",\"dish\":\"MexixcanDish3\",\"price\":200.0},{\"id\":7,\"cuisin\":\"Italian\",\"dish\":\"ItalianDish1\",\"price\":200.0},{\"id\":8,\"cuisin\":\"Italian\",\"dish\":\"ItalianDish2\",\"price\":200.0},{\"id\":9,\"cuisin\":\"Italian\",\"dish\":\"ItalianDish3\",\"price\":200.0}]";
		
		mockMvc.perform(get("/cuisins")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test get cuisin by id")
	void testGetCuisinById() throws Exception {
		var result = "{\"id\":1,\"cuisin\":\"Polish\",\"dish\":\"PolishDish1\",\"price\":200.0}";
		
		mockMvc.perform(get("/cuisins/{id}", TEST_CUISIN_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test cuisin delete")
	@DirtiesContext
	void testDeleteCuisin() throws Exception {
		mockMvc.perform(delete("/cuisins/{id}", TEST_CUISIN_ID)
				.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().is2xxSuccessful());
	}

}
